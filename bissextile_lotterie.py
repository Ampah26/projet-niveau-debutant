# -*-coding:utf-8*
import os
from random import randrange

while 1:
  try:
    choix=input("\nChoississez une option: \n1)Voir si une année est bissextile\n2)Lotterie\n")
    choix=int(choix)

    if choix==1:
     print("Débutons la conversion \n")
     annee = input("Saisissez une année supérieure à 0 :")

     try:
       annee = int(annee) 
       assert annee > 0
     except ValueError:
         print("Vous n'avez pas saisi un nombre.")
     except AssertionError:
         print("L'année saisie est inférieure ou égale à 0.")
         print("Erreur de conversion")
     else:
        if annee % 400 == 0 or (annee % 4 == 0 and annee % 100 != 0):
            print("L'année saisie est bissextile.")
        else:
            print("L'année saisie n'est pas bissextile.")
            os.system("pause")
            break

    elif choix==2:
      votre_choix=int(input("Choisir un nombre entre 1 et 10 : "))
      if votre_choix <1 or votre_choix>10:
        print("Le nombre choisi ne se trouve pas dans l'intervalle")
        os.system("pause")
        continue
      else:
         choix_gagnant = randrange(1,11)
         print("La roulette tourne... ... et s'arrête sur le numéro",choix_gagnant)
         if votre_choix==choix_gagnant:
           print("Félicitation vous êtes chanceux")
         else:
           print("\nMerci d'avoir essayé\n")
           os.system("pause")
           break
    else:
      print("\nVous devez choisir une des deux options\n")
      continue

  except ValueError:
    print("\nLes chaines de caractères et les caractères spéciaux ne sont pas valides\n")
    continue
